#ifndef TWOCOLORFADEPAGE_H
#define TWOCOLORFADEPAGE_H

#include "ledpage.h"
#include "twocolorfadeprocess.h"

class TwoColorFadePage : public LedPage
{
public:
  TwoColorFadePage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/twocolorfade";
  }

  String title() const override
  {
    return "Two Color Fade";
  }

  std::vector<String> requiredArguments() const override
  {
    return {"color", "color2", "wait"};
  }

protected:
  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new TwoColorFadeProcess(
      strip(),
      hexToRGB(arguments.arg("color")),
      hexToRGB(arguments.arg("color2")),
      arguments.arg("wait").toInt()
    );
  }

  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    String errorMessage;
    auto colorArg = arguments.arg("color");
    auto color2Arg = arguments.arg("color2");
    if (colorArg.length() != 6 || color2Arg.length() != 6)
      errorMessage += "Invalid color argument. Must be 6 digit HEX\n";
    return errorMessage;
  }

};

#endif
