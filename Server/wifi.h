#ifndef WIFI_H
#define WIFI_H

class Wifi
{
public:
  virtual void connect(const char* ssid, const char* password) = 0;
  virtual const String ip() = 0;
};

#endif
