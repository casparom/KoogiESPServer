#ifndef LEDCONTROLLER_H
#define LEDCONTROLLER_H

class LedController : public IProcess
{
private:
  NeoPixel* m_strip;
  IProcess* m_process;

public:
  LedController(NeoPixel* strip)
    : m_strip(strip)
    , m_process(new SolidColorProcess(strip, RGB()))
  {
  }

  NeoPixel* strip() const
  {
    return m_strip;
  }

  void run() override
  {
    m_process->run();
  }

  bool isSleeping() override {
    return m_process->isSleeping();
  }

  void setProcess(IProcess* process) {
    delete m_process;
    m_process = process;
  }

};

#endif
