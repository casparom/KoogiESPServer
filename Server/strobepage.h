#ifndef STROBEPAGE_H
#define STROBEPAGE_H

#include "ledpage.h"
#include "strobeprocess.h"

class StrobePage : public LedPage
{
public:
  StrobePage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/strobe";
  }

  String title() const override
  {
    return "Strobe";
  }

  std::vector<String> requiredArguments() const override
  {
    return {"color", "wait"};
  }

protected:
  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new StrobeProcess(strip(), hexToRGB(arguments.arg("color")), arguments.arg("wait").toInt());
  }

  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    String errorMessage;
    auto colorArg = arguments.arg("color");
    if (colorArg.length() != 6)
      errorMessage += "Invalid color argument. Must be 6 digit HEX\n";
    return errorMessage;
  }

};

#endif
