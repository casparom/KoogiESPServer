#ifndef SOLIDCOLORPROCESS_H
#define SOLIDCOLORPROCESS_H

#include "iprocess.h"
#include "utilitys.h"
#include "neopixel.h"

class SolidColorProcess : public IProcess
{
private:
  NeoPixel* m_strip;
  RGB m_color;
public:
  SolidColorProcess(NeoPixel* strip, RGB color)
    : m_strip(strip)
    , m_color(color)
  {
  }

  void run() override
  {
    m_strip->setAll(m_color);
    m_strip->show();
    sleepFor(100);
  }
};

#endif
