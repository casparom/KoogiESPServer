#ifndef INFOPAGE_H
#define INFOPAGE_H

#include "twocolorfadeprocess.h"

class InfoPage : public Page
{
private:
  String m_info;
public:
  InfoPage(std::vector<Page*> pages)
  {
    m_info += "\tINFO\n\n";
    m_info += "\tPAGES\n\n";
    m_info += "\tTitle\t\tURL\t\targuments\n\n";
    for (auto page : pages)
      appendPageInfo(page);
  }

  void appendPageInfo(Page* page) {
    m_info += "\t";
    m_info += page->title();
    m_info += "\t\t";
    m_info += page->url();
    m_info += "\t\t";
    for (auto argument : page->requiredArguments())
    {
      m_info += argument;
      m_info += ", ";
    }
    m_info = m_info.substring(0, m_info.length()-2);
    m_info += "\n";
  }

  String url() const override
  {
    return "/info";
  }

  String title() const override
  {
    return "Info";
  }

  std::vector<String> requiredArguments() const
  {
    return std::vector<String>();
  }

protected:
  void process(const Arguments& arguments) { }

  String getArgumentLimitErrors(const Arguments& arguments) const
  {
    return "";
  }

  String getSuccessMessage(WifiWebServer* server) const override
  {
    String message = title() + "\n\n";
    message += getRequestInfo(server);
    message += "\n";
    message += m_info;
    return message;
  }

};

#endif
