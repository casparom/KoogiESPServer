#ifndef SILINDERROTATEPAGE_H
#define SILINDERROTATEPAGE_H

#include "ledpage.h"
#include "silinderrotateprocess.h"

class SilinderRotatePage : public LedPage
{
public:
  SilinderRotatePage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/silinderrotate";
  }

  String title() const override
  {
    return "Silinder rotate";
  }

  std::vector<String> requiredArguments() const override
  {
    return {"color", "wait"};
  }

protected:
  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new SilinderRotateProcess(strip(), 16.5, hexToRGB(arguments.arg("color")), arguments.arg("wait").toInt());
  }

  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    String errorMessage;
    auto colorArg = arguments.arg("color");
    if (colorArg.length() != 6)
      errorMessage += "Invalid color argument. Must be 6 digit HEX\n";
    return errorMessage;
  }

};

#endif
