#ifndef PROCESSRUNNER_H
#define PROCESSRUNNER_H

#include "solidcolorprocess.h"

class ProcessRunner
{
private:
  std::vector<IProcess*> m_processes;
public:

  void addProcess(IProcess* process)
  {
    m_processes.push_back(process);
  }

  void runProcesses()
  {
    for (auto process : m_processes)
    {
      if (!process->isSleeping()) {
        process->run();
      }
    }
    delay(0);

  }
};

#endif
