#ifndef ESP32WEBSERVERADAPTER_H
#define ESP32WEBSERVERADAPTER_H

#include "wifiwebserver.h"
#include "esp32updateserver.h"

#include <WebServer.h>
#include <ESPmDNS.h>

class ESP32WebServerAdapter : public WifiWebServer {
private:
  WebServer m_server;
  ESP32UpdateServer m_updater;

public:
  ESP32WebServerAdapter()
    : m_server(80)
  {
    m_updater.setup(&m_server);
  }

  void begin() override {
    MDNS.begin("esp");
    m_server.begin();
  }

  void service() override  {
    m_server.handleClient();
  }

  void onNotFound(THandlerFunction fn) override  {
    m_server.onNotFound(fn);
  }

  void on(const String &uri, THandlerFunction handler) override  {
    m_server.on(uri, handler);
  }

  const String& uri() {
    return m_server.uri();
  }

  void send(int code, char* content_type, const String& content) override  {
    m_server.send(code, content_type, content);
  }

  const Arguments args() override  {
    std::map<String, String> map;
    for (int i=0; i < m_server.args(); i++)
      map.emplace(m_server.argName(i), m_server.arg(i));
    return Arguments::fromMap(map);
  }
};

#endif
