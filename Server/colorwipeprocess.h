#ifndef COLORWIPEPROCESS_H
#define COLORWIPEPROCESS_H

#include "iprocess.h"
#include "utilitys.h"
#include "neopixel.h"

const RGB offColor = RGB();

class ColorWipeProcess : public IProcess
{
private:
  NeoPixel* m_strip;
  RGB m_color;
  int m_wait;
  int m_iterator;
  bool m_isReverse;
public:
  ColorWipeProcess(NeoPixel* strip, RGB color, int wait)
    : m_strip(strip)
    , m_color(color)
    , m_wait(wait)
    , m_iterator(0)
    , m_isReverse(false)
  {
  }
  
  void run() override
  {
    if (m_iterator >= m_strip->numPixels())
    {
      m_iterator = 0;
      m_isReverse = !m_isReverse;
    }
    for(int i=0; i<m_strip->numPixels(); i++)
    {
      if (i < m_iterator)
        m_strip->setPixelColor(i, m_isReverse ? offColor : m_color);
      else
        m_strip->setPixelColor(i, m_isReverse ? m_color : offColor);
    }
    m_strip->show();
    sleepFor(m_wait);
    m_iterator++;
  }
};

#endif
