#ifndef SERVER_H
#define SERVER_H

#include "processrunner.h"
#include "rootpage.h"
#include "solidcolorpage.h"
#include "colorwipepage.h"
#include "rainbowpage.h"
#include "twocolorfadepage.h"
#include "strobepage.h"
#include "silinderrotatepage.h"
#include "infopage.h"
#include "neopixel.h"
#include "ledcontroller.h"
#include "ledwebserver.h"

#ifdef ESP8266
    #include "esp8266wifi.h"
    #include "esp8266webserveradapter.h"

    LedWebServer server(
      new ESP8266Wifi(),
      new ESP8266WebServerAdapter()
    );
#endif

#ifdef ESP32
    #include "esp32wifi.h"
    #include "esp32webserveradapter.h"

    LedWebServer server(
      new ESP32Wifi(),
      new ESP32WebServerAdapter()
    );
#endif

NeoPixel strip(60);
LedController ledController(&strip);
ProcessRunner processRunner;

void setup(void)
{
  Serial.begin(115200);
  Serial.println("Booting...");

  strip.setUp();

  setupServer();

  processRunner.addProcess(&server);
  processRunner.addProcess(&ledController);

  #ifdef ESP8266
    printResetInfo();
  #endif
}

void setupServer()
{
  server.addPage(new RootPage(&ledController));
  server.addPage(new SolidColorPage(&ledController));
  server.addPage(new ColorWipePage(&ledController));
  server.addPage(new RainbowPage(&ledController));
  server.addPage(new TwoColorFadePage(&ledController));
  server.addPage(new StrobePage(&ledController));
  server.addPage(new SilinderRotatePage(&ledController));
  server.addPage(new InfoPage(server.pages()));
  server.start();
}

void loop(void)
{
  noInterrupts();
  while(1) {
    try {
      processRunner.runProcesses();
      #ifdef ESP8266
        ESP.wdtFeed();
      #endif
      delay(0);
    } catch (const std::exception& e) {
      Serial.println(e.what());
    } catch (...) {
      Serial.println("Unknown exception");
    }

  }
}

#ifdef ESP8266
  void printResetInfo()
  {
    rst_info* rtc_info = ESP.getResetInfoPtr();
    Serial.print("reset reason: ");
    Serial.println(rtc_info->reason);
    if (rtc_info->reason == REASON_WDT_RST || rtc_info->reason == REASON_EXCEPTION_RST || rtc_info->reason == REASON_SOFT_WDT_RST) {
      if (rtc_info->reason == REASON_EXCEPTION_RST) {
        Serial.print("Fatal exception: ");
        Serial.println(rtc_info->exccause);
      }
      Serial.printf("epc1=0x%08x, epc2=0x%08x, epc3=0x%08x, excvaddr=0x%08x, depc=0x%08x\n", rtc_info->epc1, rtc_info->epc2, rtc_info->epc3, rtc_info->excvaddr, rtc_info->depc);//The address of the last crash is printed, which is used to debug garbled output.
    }
  }
#endif

#endif
