#ifndef RAINBOWPROCESS_H
#define RAINBOWPROCESS_H

#include "iprocess.h"
#include "utilitys.h"
#include "neopixel.h"

class RainbowProcess : public IProcess
{
private:
  NeoPixel* m_strip;
  int m_wait;
  int m_iterator;
public:
  RainbowProcess(NeoPixel* strip, int wait)
    : m_strip(strip)
    , m_wait(wait)
    , m_iterator(0)
  {
  }

  void run() override
  {
    if (m_iterator >= 256)
      m_iterator = 0;
    for(int i=0; i<m_strip->numPixels(); i++) {
      m_strip->setPixelColor(i, wheelLedColor((i+m_iterator) & 255));
    }
    m_strip->show();
    sleepFor(m_wait);
    m_iterator++;
  }
};

#endif
