#ifndef ESP32WIFI_H
#define ESP32WIFI_H

#include "wifi.h"

#include <WiFi.h>

class ESP32Wifi : public Wifi
{
public:
  void connect(const char* ssid, const char* password) override
  {
    WiFi.mode(WIFI_AP_STA);
    while (!isWifiConnected()) {
      WiFi.begin(ssid, password);
      Serial.println("WiFi failed, retrying.");
      delay(200);
    }
  }

  const String ip()
  {
    return WiFi.localIP().toString();
  }

private:
  bool isWifiConnected()
  {
    return WiFi.waitForConnectResult() == WL_CONNECTED;
  }
};

#endif
