#ifndef PAGE_H
#define PAGE_H

#include "wifiwebserver.h"

#include <vector>
#include <map>

class Page
{
public:
  virtual String url() const = 0;

  virtual void handleRequest(WifiWebServer* server)
  {
    auto arguments = server->args();
    String argumentErrors = getArgumentErrors(arguments);
    if (argumentErrors != String())
    {
      sendErrorMessage(server, argumentErrors);
      return;
    }
    process(arguments);
    sendSuccessMessage(server);
  }

  virtual String title() const = 0;
  virtual std::vector<String> requiredArguments() const = 0;

protected:
  virtual void process(const Arguments& arguments) = 0;
  virtual String getArgumentLimitErrors(const Arguments& arguments) const = 0;

  String getArgumentErrors(const Arguments& arguments) const
  {
    String message;
    auto missingArguments = getMissingArguments(arguments);
    if (!missingArguments.empty())
    {
      message += "Not all required arguments were given: ";
      for (auto a : missingArguments)
        message += "'" + a + "' ";
      message += "\n";
    }
    message += getArgumentLimitErrors(arguments);
    return message;
  }

  std::vector<String> getMissingArguments(const Arguments& arguments) const
  {
    std::vector<String> missingArguments;
    for (auto argument : requiredArguments())
      if (!arguments.hasArg(argument))
        missingArguments.push_back(argument);
    return missingArguments;
  }

  void sendErrorMessage(WifiWebServer* server, const String& errorMessage)
  {
    String message = title() + "\n" + errorMessage + "\n\n";
    message += String(getRequestInfo(server));
    server->send(404, "text/plain", message);
  }

  String getRequestInfo(WifiWebServer* server) const
  {
    auto args = server->args();

    String message = "URI: ";
    message += server->uri();
    message += "\nMethod: ";
    message += "\nArguments: ";
    message += args.count();
    message += "\n";
    for (auto i=0; i < args.count(); i++)
      message += " " + args.argName(i) + ": " + args.arg(i) + "\n";
    return message;
  }

  void sendSuccessMessage(WifiWebServer* server)
  {
    auto message = getSuccessMessage(server);
    server->send(200, "text/plain", message);
  }

  virtual String getSuccessMessage(WifiWebServer* server) const
  {
    String message = title() + "\n\n";
    message += getRequestInfo(server);
    return message;
  }

};

#endif
