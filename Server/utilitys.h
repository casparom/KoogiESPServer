#ifndef UTILITYS_H
#define UTILITYS_H

#include "rgb.h"
#include <stdlib.h>

RGB hexToRGB(String hexStr){
  if (hexStr.length() != 6){
    return RGB();
  }
  long number = (long) strtol( &hexStr[0], NULL, 16);
  return RGB(number >> 16, number >> 8 & 0xFF, number & 0xFF);
}

RGB wheelLedColor(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  int red = 0;
  int green = 0;
  int blue = 0;
  if(WheelPos < 85)
  {
    red = 255-WheelPos*3;
    blue = WheelPos*3;
  }
  else if(WheelPos < 170)
  {
    WheelPos -= 85;
    green = WheelPos*3;
    blue = 255-WheelPos*3;
  }
  else
  {
    WheelPos -= 170;
    red = WheelPos*3;
    green = 255-WheelPos*3;
  }
  return RGB(red, green, blue);
}

int stepSizeTimesHundred(int value1, int value2, int numOfSteps)
{
  return abs(value1 - value2)*100/numOfSteps;
}

#endif
