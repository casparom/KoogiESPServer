#ifndef STROBEPROCESS_H
#define STROBEPROCESS_H

#include "iprocess.h"
#include "utilitys.h"
#include "neopixel.h"

class StrobeProcess : public IProcess
{
private:
  NeoPixel* m_strip;
  RGB m_color;
  int m_wait;
  bool m_isOn;
public:
  StrobeProcess(NeoPixel* strip, RGB color, int wait)
    : m_strip(strip)
    , m_color(color)
    , m_wait(wait)
    , m_isOn(false)
  {
  }
  
  void run() override
  {
    RGB currentColor = m_isOn ? m_color : RGB();
    m_strip->setAll(currentColor);
    m_strip->show();
    m_isOn = !m_isOn;
    sleepFor(m_wait);
  }
};

#endif
