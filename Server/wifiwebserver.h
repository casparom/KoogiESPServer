#ifndef WIFIWEBSERVER_H
#define WIFIWEBSERVER_H

#include "arguments.h"

#include <map>
#include <functional>

typedef std::function<void(void)> THandlerFunction;

class WifiWebServer {
public:
  virtual void begin() = 0;
  virtual void service() = 0;

  virtual void onNotFound(THandlerFunction fn) = 0;
  virtual void on(const String &uri, THandlerFunction handler) = 0;

  virtual const String& uri() = 0;

  virtual void send(int code, char* content_type, const String& content) = 0;

  virtual const Arguments args() = 0;
};

#endif
