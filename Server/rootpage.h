#ifndef ROOTPAGE_H
#define ROOTPAGE_H

#include "ledpage.h"
#include "solidcolorprocess.h"

class RootPage : public LedPage
{
public:
  RootPage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/";
  }

  String title() const override
  {
    return "LEDs OFF";
  }

  std::vector<String> requiredArguments() const override
  {
    return {};
  }

protected:
  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    return "";
  }

  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new SolidColorProcess(strip(), RGB());
  }

};

#endif
